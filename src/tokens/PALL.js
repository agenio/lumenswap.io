import logo from 'src/assets/images/pall.png';

export default {
  code: 'PALL',
  logo,
  web: 'www.mintx.com',
  issuer: 'GCQ5ZYECTNYW6BZ47AZN6M5BXKV7ZZ24XPNHPSYZVUDVK7CIITOMPALL',
};
