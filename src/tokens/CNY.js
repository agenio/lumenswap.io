import logo from 'src/assets/images/cny.png';

export default {
  code: 'CNY',
  logo,
  web: 'ripplefox.com',
  issuer: 'GAREELUB43IRHWEASCFBLKHURCGMHE5IF6XSE7EXDLACYHGRHM43RFOX',
};
