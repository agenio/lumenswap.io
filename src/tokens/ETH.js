import logo from 'src/assets/images/eth-logo.png';

export default {
  code: 'ETH',
  logo,
  web: 'apay.io',
  issuer: 'GBDEVU63Y6NTHJQQZIKVTC23NWLQVP3WJ2RI2OTSJTNYOIGICST6DUXR',
};
