export const trsStatus = {
  SUCCESS: 'success',
  FAIL: 'fail',
  WARNING: 'WARNING',
};

export const connectModalTab = {
  CONNECT: 'connect',
  PUBLIC: 'public',
  PRIVATE: 'private',
  CONNECTING_LEDGER: 'CONNECTING_LEDGER',
  CONNECTING_ALBEDO: 'CONNECTING_ALBEDO',
  CONNECTING_FREIGHTER: 'CONNECTING_FREIGHTER',
  CONNECTING_TREZOR: 'CONNECTING_TREZOR',
};
